package testapp.chandmahame.com.chandmahame.model;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface Api {
    @GET("diaries")
    Single<List<DiaryModel>> getDiaries();

    @GET("notification")
    Single<NotificationModel> getNotification();
}
