package testapp.chandmahame.com.chandmahame.util;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import testapp.chandmahame.com.chandmahame.model.ApiService;
import testapp.chandmahame.com.chandmahame.model.DiaryModel;
import testapp.chandmahame.com.chandmahame.model.NotificationModel;

public class PeriodicWork extends Worker {
    private ApiService apiService = new ApiService();
    private CompositeDisposable disposable = new CompositeDisposable();

    public PeriodicWork(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        //  fetchFromRemote(); fetch data from server
        showMockData();
        return Result.success();
    }

    public void fetchFromRemote() {
        final Result[] returnValue = new Result[1];
        disposable.add(
                apiService.getNotification()
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<NotificationModel>() {
                            @Override
                            public void onSuccess(NotificationModel notificationModel) {
                                NotificationsHelper.getInstance(getApplicationContext()).createNotification(notificationModel);

                            }

                            @Override
                            public void onError(Throwable e) {

                            }
                        })
        );
    }

    public void showMockData() {
        String json = loadJSONFromAsset(getApplicationContext());
        Gson gson = new Gson();

        NotificationsHelper.getInstance(getApplicationContext()).createNotification(gson.fromJson(json, NotificationModel.class));

    }

    public String loadJSONFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("mock_notification_data.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }
}
