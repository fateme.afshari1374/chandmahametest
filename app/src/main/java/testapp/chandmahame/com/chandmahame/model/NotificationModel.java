package testapp.chandmahame.com.chandmahame.model;

import com.google.gson.annotations.SerializedName;

public class NotificationModel {

    public int id;

    @SerializedName("big_text")
    public String bigText;

    @SerializedName("big_content_title")
    public String bigContentTitle;

    @SerializedName("summary_text")
    public String summaryText;

    @SerializedName("content_title")
    public String contentTitle;

    public String path;

    public NotificationModel(int id, String bigText, String bigContentTitle, String summaryText, String contentTitle, String path) {
        this.id = id;
        this.bigText = bigText;
        this.bigContentTitle = bigContentTitle;
        this.summaryText = summaryText;
        this.contentTitle = contentTitle;
        this.path = path;
    }

}
