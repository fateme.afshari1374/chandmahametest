package testapp.chandmahame.com.chandmahame.util;

import android.app.Application;

import java.util.concurrent.TimeUnit;

import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

public class application extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest.Builder(PeriodicWork.class, 1, TimeUnit.HOURS).build();
        WorkManager.getInstance(getApplicationContext()).enqueue(periodicWorkRequest);
    }


}
