package testapp.chandmahame.com.chandmahame.view;


import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import testapp.chandmahame.com.chandmahame.R;
import testapp.chandmahame.com.chandmahame.databinding.ItemDiaryBinding;
import testapp.chandmahame.com.chandmahame.model.DiaryModel;

public class DiaryListAdapter
        extends RecyclerView.Adapter<DiaryListAdapter.ViewHolder> {

    private ArrayList<DiaryModel> diaryModelArrayList;

    public DiaryListAdapter(ArrayList<DiaryModel> diaryModelArrayList) {
        this.diaryModelArrayList = diaryModelArrayList;
    }

    public void updateList(List<DiaryModel> newDiaryList) {
        diaryModelArrayList.clear();
        diaryModelArrayList.addAll(newDiaryList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemDiaryBinding view = DataBindingUtil.inflate(inflater, R.layout.item_diary, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemView.setDiary(diaryModelArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return diaryModelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ItemDiaryBinding itemView ;
        public ViewHolder(@NonNull ItemDiaryBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;

        }
    }
}
