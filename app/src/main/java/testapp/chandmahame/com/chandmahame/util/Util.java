package testapp.chandmahame.com.chandmahame.util;


import android.content.Context;
import android.widget.ImageView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;

import androidx.databinding.BindingAdapter;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import testapp.chandmahame.com.chandmahame.R;

public class Util {
    @BindingAdapter("android:imageUrl")
    public static void loadImage(ImageView view, String url) {
        RequestOptions options = new RequestOptions()
                .placeholder(getProgressDrawable(view.getContext()))
                .error(R.mipmap.ic_launcher);

        Glide.with(view.getContext())
                .setDefaultRequestOptions(options)
                .load(url)
                .into(view);
    }

    public static CircularProgressDrawable getProgressDrawable(Context context) {
        CircularProgressDrawable cpd = new CircularProgressDrawable(context);
        cpd.setStrokeWidth(10f);
        cpd.setCenterRadius(50f);
        cpd.start();
        return cpd;
    }

}
