package testapp.chandmahame.com.chandmahame.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import testapp.chandmahame.com.chandmahame.R;
import testapp.chandmahame.com.chandmahame.model.NotificationModel;
import testapp.chandmahame.com.chandmahame.view.Camera;
import testapp.chandmahame.com.chandmahame.view.MainActivity;

public class NotificationsHelper {

    //Any name of String and any number for the following two variables
    private static final String CHANNEL_ID = "Channel ID";

    private Context context;
    private static NotificationsHelper instance;

    private NotificationsHelper(Context context) {
        this.context = context;
    }

    public static NotificationsHelper getInstance(Context context) {
        if (instance == null) {
            instance = new NotificationsHelper(context);
        }
        return instance;
    }

    public void createNotification(NotificationModel model) {
        createNotificationChannel();


        PendingIntent pendingIntent =createPendingIntent(model.path);

        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);

        Notification notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(model.contentTitle)
                .setContentText(model.bigText)
                .setSubText(model.summaryText)
                .setStyle(
                        new NotificationCompat.BigPictureStyle()
                                .bigPicture(icon)
                                .bigLargeIcon(null)
                )
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .build();

        NotificationManagerCompat.from(context).notify(model.id, notification);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String name = CHANNEL_ID;
            String description = "Dogs retrieved notification channel";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private PendingIntent createPendingIntent(String path) {
        Intent intent;
        switch (path) {
            case "camera":
                intent = new Intent(context, Camera.class);

                break;
            case "main":
                intent = new Intent(context, MainActivity.class);
                break;
            default:
                intent = new Intent(context, MainActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return PendingIntent.getActivity(context, 0, intent, 0);
    }
}
