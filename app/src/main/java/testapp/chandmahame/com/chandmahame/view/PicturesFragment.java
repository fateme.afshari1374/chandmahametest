package testapp.chandmahame.com.chandmahame.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import testapp.chandmahame.com.chandmahame.R;
import testapp.chandmahame.com.chandmahame.viewmodel.PictureViewModel;


/**
 * A placeholder fragment containing a simple view.
 */
public class PicturesFragment extends Fragment {
    RecyclerView list;
    TextView lableEmpty ;
    private PictureViewModel viewModel;
    private PictureListAdapter picsListAdapter = new PictureListAdapter(new ArrayList<>());

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_pictures, container, false);
        list = (RecyclerView) root.findViewById(R.id.list_pictures);
        lableEmpty = (TextView)root.findViewById(R.id.lbl_empty);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(PictureViewModel.class);
        viewModel.getData();
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.setAdapter(picsListAdapter);
        observeViewModel();
    }

    private void observeViewModel() {
        viewModel.pics.observe(this, pictureModels -> {
            if (pictureModels != null && pictureModels.size()>0 && pictureModels instanceof List) {
                list.setVisibility(View.VISIBLE);
                picsListAdapter.updateList(pictureModels);
                lableEmpty.setVisibility(View.GONE);

            }else {
                lableEmpty.setVisibility(View.VISIBLE);
            }
        });
    }

}