package testapp.chandmahame.com.chandmahame.view;


import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import testapp.chandmahame.com.chandmahame.R;

import testapp.chandmahame.com.chandmahame.databinding.ItemListBinding;
import testapp.chandmahame.com.chandmahame.model.PictureModel;

public class PictureListAdapter
        extends RecyclerView.Adapter<PictureListAdapter.ViewHolder> {

    private ArrayList<PictureModel> pictureModelArrayList;

    public PictureListAdapter(ArrayList<PictureModel> pictureModelArrayList) {
        this.pictureModelArrayList = pictureModelArrayList;
    }

    public void updateList(List<PictureModel> newPicList) {
        pictureModelArrayList.clear();
        pictureModelArrayList.addAll(newPicList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemListBinding view = DataBindingUtil.inflate(inflater, R.layout.item_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemView.setPic(pictureModelArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return pictureModelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ItemListBinding itemView ;
        public ViewHolder(@NonNull ItemListBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;

        }
    }
}
