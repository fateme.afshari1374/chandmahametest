package testapp.chandmahame.com.chandmahame.viewmodel;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import testapp.chandmahame.com.chandmahame.model.ApiService;
import testapp.chandmahame.com.chandmahame.model.DiaryModel;

public class DiaryViewModel extends AndroidViewModel {
    public MutableLiveData<List<DiaryModel>> diaries = new MutableLiveData<List<DiaryModel>>();
    public MutableLiveData<Boolean> dogLoadError = new MutableLiveData<Boolean>();
    public MutableLiveData<Boolean> loading = new MutableLiveData<Boolean>();

    private ApiService apiService = new ApiService();
    private CompositeDisposable disposable = new CompositeDisposable();

    public DiaryViewModel(@NonNull Application application) {
        super(application);
    }

    public void fetchFromRemote() {
        loading.setValue(true);
        disposable.add(
                apiService.getDiaries()
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<List<DiaryModel>>() {
                            @Override
                            public void onSuccess(List<DiaryModel> diaryModels) {
                                diaries.setValue(diaryModels);
                                dogLoadError.setValue(false);
                                loading.setValue(false);
                            }

                            @Override
                            public void onError(Throwable e) {
                                dogLoadError.setValue(true);
                                loading.setValue(false);
                                e.printStackTrace();
                            }
                        })
        );
    }

    public void showMockData() {
        String json = loadJSONFromAsset(getApplication());
        Gson gson = new Gson() ;
        Type type = new TypeToken<List<DiaryModel>>(){}.getType();
        diaries.setValue(gson.fromJson(json, type));
        dogLoadError.setValue(false);
        loading.setValue(false);
    }

    public String loadJSONFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("mockdata.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }
}
