package testapp.chandmahame.com.chandmahame.model;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    public static final String BASE_URL = "https://base_url";
    private Api api;

    public ApiService() {
        api = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(Api.class);
    }

    public Single<List<DiaryModel>> getDiaries() {
        return api.getDiaries();
    }
    public Single<NotificationModel> getNotification() {
        return api.getNotification();
    }
}
