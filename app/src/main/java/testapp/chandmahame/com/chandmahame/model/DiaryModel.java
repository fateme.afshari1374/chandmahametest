package testapp.chandmahame.com.chandmahame.model;

public class DiaryModel {
    public int id;
    public String imagePath;
    public String title;

    public DiaryModel(int id, String imagePath, String title) {
        this.title = title;
        this.imagePath = imagePath;
        this.id = id;
    }
}
