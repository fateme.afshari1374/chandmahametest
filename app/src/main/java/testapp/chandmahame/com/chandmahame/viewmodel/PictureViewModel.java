package testapp.chandmahame.com.chandmahame.viewmodel;

import android.app.Application;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import testapp.chandmahame.com.chandmahame.model.PictureModel;

public class PictureViewModel extends AndroidViewModel {

    public MutableLiveData<List<PictureModel>> pics = new MutableLiveData<List<PictureModel>>();
    public MutableLiveData<Boolean> loading = new MutableLiveData<Boolean>();
    private AsyncTask<Void, Void, List<PictureModel>> retrieveTask;

    public PictureViewModel(@NonNull Application application) {
        super(application);
    }

    public void getData() {
        loading.setValue(true);
        retrieveTask = new RetrieveDogsTask();
        retrieveTask.execute();
    }

    private void dataRetrived(List<PictureModel> pictureModels) {
        pics.setValue(pictureModels);
        loading.setValue(false);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (retrieveTask != null) {
            retrieveTask.cancel(true);
            retrieveTask = null;
        }
    }

    private class RetrieveDogsTask extends AsyncTask<Void, Void, List<PictureModel>> {


        @Override
        protected List<PictureModel> doInBackground(Void... voids) {
            List<PictureModel> pictureModelList = new ArrayList<PictureModel>();
            File directory = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), "chandmahame");
            File newfile[] = directory.listFiles();
            if (directory.exists() && newfile != null) {

                PictureModel pictureModel;

                for (int i = 0; i < newfile.length; i++) {
                    pictureModel = new PictureModel(newfile[i].getAbsolutePath(), newfile[i].getName());
                    pictureModelList.add(pictureModel);
                }
            }
            return pictureModelList;

        }

        @Override
        protected void onPostExecute(List<PictureModel> pictureModels) {
            dataRetrived(pictureModels);

        }
    }
}