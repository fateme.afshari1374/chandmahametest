package testapp.chandmahame.com.chandmahame.model;

public class PictureModel {
    public String Uri;
    public String imageName;

    public PictureModel(String Uri, String imageName) {
        this.imageName = imageName;
        this.Uri = Uri;
    }
}
