package testapp.chandmahame.com.chandmahame.view;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import testapp.chandmahame.com.chandmahame.R;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class Camera extends AppCompatActivity {
    static final int REQUEST_TAKE_PHOTO = 1;
    private static final int PERMISSION_CAMERA = 2;
    private static final int PERMISSION_EXTERNAL_STORAGE = 3;
    private android.hardware.Camera mCamera;
    private CameraPreview mPreview;
    ImageButton accepButton;
    ImageButton cancleButton;
    FloatingActionButton captureButton;
    byte[] picture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        accepButton = (ImageButton) findViewById(R.id.button_accept);
        cancleButton = (ImageButton) findViewById(R.id.button_cancle);
        captureButton = (FloatingActionButton) findViewById(R.id.button_capture);
        checkCameraPermission();
        accepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkStoragePermission();
            }
        });
        cancleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCamera.startPreview();
                accepButton.setVisibility(View.INVISIBLE);
                cancleButton.setVisibility(View.INVISIBLE);
                captureButton.setClickable(true);
                captureButton.setFocusable(true);
            }
        });
    }

    public void checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                new AlertDialog.Builder(this)
                        .setTitle("Camera permission")
                        .setMessage("This app requires access to camera.")

                        .setPositiveButton("ازم بپرس", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                requestCameraPermission();
                            }
                        })

                        .setNegativeButton("نه!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                notifyPermission(false);
                            }
                        })
                        .show();
            } else {
                requestCameraPermission();
            }
        } else {
            notifyPermission(true);
        }
    }

    public void checkStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                new AlertDialog.Builder(this)
                        .setTitle("save permission")
                        .setMessage("This app requires access to external storage.")

                        .setPositiveButton("ازم بپرس", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                requestExternalStoragePermission();
                            }
                        })

                        .setNegativeButton("نه!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                notifyStorgePermission(false);
                            }
                        })
                        .show();
            } else {
                requestExternalStoragePermission();
            }
        } else {
            notifyStorgePermission(true);
        }
    }

    private void requestCameraPermission() {
        String[] permissions = {Manifest.permission.CAMERA};
        ActivityCompat.requestPermissions(this, permissions, PERMISSION_CAMERA);
    }

    private void requestExternalStoragePermission() {
        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        ActivityCompat.requestPermissions(this, permissions, PERMISSION_EXTERNAL_STORAGE);
    }

    private void notifyStorgePermission(boolean status) {
        if(status){
            File pictureFile = getOutputMediaFile();
            mCamera.startPreview();
            if (pictureFile == null) {
                Log.d("TAG", "Error creating media file, check storage permissions");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(picture);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.d("TAG", "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d("TAG", "Error accessing file: " + e.getMessage());
            }
            accepButton.setVisibility(View.INVISIBLE);
            cancleButton.setVisibility(View.INVISIBLE);
            captureButton.setClickable(true);
            captureButton.setFocusable(true);
            navigateToMainActivity();
        }else {
            Toast.makeText(getApplicationContext() , "شما دسترسی ذخیره عکس را نداده اید .",Toast.LENGTH_LONG).show();
        }

    }

    private void notifyPermission(boolean status) {
        if (status) {
            mCamera = getCameraInstance();
            mPreview = new CameraPreview(this, mCamera);
            FrameLayout preview = (FrameLayout) findViewById(R.id.camera_peview);
            preview.addView(mPreview);


            captureButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // get an image from the camera
                            mCamera.takePicture(null, null, mPicture);
                            captureButton.setClickable(false);
                            captureButton.setFocusable(false);

                        }
                    }
            );
        } else {
            finish();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    notifyPermission(true);
                } else {
                    notifyPermission(false);
                }
                break;
            }
            case PERMISSION_EXTERNAL_STORAGE : {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    notifyStorgePermission(true);
                } else {
                    notifyStorgePermission(false);
                }
            }
            break;
        }
    }

    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public static android.hardware.Camera getCameraInstance() {
        android.hardware.Camera c = null;
        try {
            c = android.hardware.Camera.open();// attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    private android.hardware.Camera.PictureCallback mPicture = new android.hardware.Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, android.hardware.Camera camera) {
            picture = data;
            accepButton.setVisibility(View.VISIBLE);
            cancleButton.setVisibility(View.VISIBLE);

        }
    };

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "chandmahame");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("chandmahame", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");


        return mediaFile;
    }

    private void navigateToMainActivity() {
        Intent intent = new Intent(Camera.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        navigateToMainActivity();
    }
}
