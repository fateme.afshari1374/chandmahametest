package testapp.chandmahame.com.chandmahame.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import testapp.chandmahame.com.chandmahame.R;
import testapp.chandmahame.com.chandmahame.viewmodel.DiaryViewModel;
import testapp.chandmahame.com.chandmahame.viewmodel.PictureViewModel;

public class DiariesFragment extends Fragment {
    private DiaryViewModel diaryViewModel;
    RecyclerView diaryList;
    private DiaryListAdapter diaryListAdapter = new DiaryListAdapter(new ArrayList<>());

    public DiariesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_diaries, container, false);
        diaryList = (RecyclerView) root.findViewById(R.id.list_diaries);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        diaryViewModel = ViewModelProviders.of(this).get(DiaryViewModel.class);
        // diaryViewModel.fetchFromRemote();     get data from server
        diaryViewModel.showMockData();
        diaryList.setLayoutManager(new LinearLayoutManager(getContext()));
        diaryList.setAdapter(diaryListAdapter);
        observeViewModel();
    }

    private void observeViewModel() {
        diaryViewModel.diaries.observe(this, diaryModels -> {
            if (diaryModels != null && diaryModels instanceof List) {
                diaryList.setVisibility(View.VISIBLE);
                diaryListAdapter.updateList(diaryModels);
            }
        });
    }
}